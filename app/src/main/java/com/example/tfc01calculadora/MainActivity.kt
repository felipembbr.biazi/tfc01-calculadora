package com.example.tfc01calculadora

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    lateinit var DI: TextView
    lateinit var DH: TextView
    var lastDot: Boolean = false
    //    modo resultado = true - modo calculo = false
    var estado = true
    var n1 = 0.0
    var n2 = 0.0
    var op = ""
    var aux = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar!!.hide()
        estado = true
        DI = findViewById(R.id.DisplayInput)
        DH = findViewById(R.id.DisplayHistorico)
    }

    fun acaoNumero (v: View){
        if (estado) {
            DI.text = (v as Button).text
            estado = false
        }
        else {
            DI.append((v as Button).text)
        }
    }
    fun acaoApagar (v: View){
        var temp = DI.text

        if(temp.length == 1) {
            DI.text = temp.replaceFirst(".$".toRegex(), "0")
            estado = true
            n2 = 0.0
            aux = true
        }
        else{
            DI.text = temp.replaceFirst(".$".toRegex(), "")
        }

    }
    fun acaoPonto (v: View){
        var temp = DI.text
        var i = 0

        while(i < temp.length){
            if (temp.get(i).toString() == "."){
                temp = "."
            }
            i++
        }

        if(temp != "."){
            if (estado) {
                DI.text = "0"
                estado = false
            }
            else {
                DI.append((v as Button).text)
            }

        }
    }
    fun acaoLimpar (v: View){
        DH.text = ""
        DI.text = "0"
        estado = true
        n1 = 0.0
        n2 = 0.0
        op = ""
    }
    fun acaoOperador (v: View){
        if (!estado) {
            if (op == ""){
                n1 = DI.text.toString().toDouble()
                op = (v as Button).text.toString()
                DH.append(" $n1 $op")
            }
            else {
                n2 = DI.text.toString().toDouble()
                n1 = when (op) {
                    "+" -> n1 + n2
                    "÷" -> n1 / n2
                    "×" -> n1 * n2
                    "-" -> n1 - n2
                    else -> n1
                }
                n2 = 0.0
                op = (v as Button).text.toString()
                DI.text = "$n1"
                DH.text = "$n1 $op"
            }
        }
        else {
            if (op != "") {
                n1 = DI.text.toString().toDouble()
                op = (v as Button).text.toString()
                DH.text = (" $n1 $op")
            }
        }
        estado = true
    }
    fun acaoIgual (v: View){
        if (!estado){
            if (op != "") {
                n2 = DI.text.toString().toDouble()
                DH.text = "$n1 $op "
                n1 = when (op) {
                    "+" -> n1 + n2
                    "÷" -> n1 / n2
                    "×" -> n1 * n2
                    "-" -> n1 - n2
                    else -> n1
                }
                DI.text = "$n1"
                DH.append("$n2 = $n1")
            }
        }
        if (estado && n2 != 0.0){
            DH.text = "$n1 $op "
            n1 = when (op) {
                "+" -> n1 + n2
                "÷" -> n1 / n2
                "×" -> n1 * n2
                "-" -> n1 - n2
                else -> n1
            }
            DI.text = "$n1"
            DH.append("$n2 = $n1")
        }
        if (estado && op != "" && n2 == 0.0) {
            n2 = DI.text.toString().toDouble()
            if(!aux){
                n1 = n2
            }

            DH.text = "$n1 $op "
            n1 = when (op) {
                "+" -> n1 + n2
                "÷" -> n1 / n2
                "×" -> n1 * n2
                "-" -> n1 - n2
                else -> n1
            }
            DI.text = "$n1"
            DH.append("$n2 = $n1")
        }
        aux = false
        estado = true
    }

}